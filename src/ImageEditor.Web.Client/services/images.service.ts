﻿import { Injectable } from '@angular/core';
import { Http, Response as HttpResponse, Headers, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs/Observable';

import "rxjs/add/operator/map";
import 'rxjs/add/operator/catch';

import Response from "../models/response";

/**
 * Путь к контроллеру изображений.
 */
const CONTROLLER_URL = "/api/images";

/**
 * Сервис запросов изображений.
 */
@Injectable()
export default class ImagesService {
    /**
     * 1Опции с заголовок.
     */
    private _options: RequestOptions = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });

    /**
     * Конструктор.
     * @param {Http} http Сервис осуществления запросов.
     */
    constructor(protected _http: Http) {
        this._http = _http;
    }

    /**
     * Сохранение ихображения.
     * @param {sting} imageData Изображение в двоичном формате.
     */
    public put(imageData: string): Observable<Response> {
        let firstCommaIndex = imageData.indexOf(',');
        let data = imageData.substring(firstCommaIndex + 1);

        return this._http
            .put(CONTROLLER_URL, JSON.stringify({ imageData: data }), this._options)
            .map(this.extractData);

    }

    /**
     * Извлечение данных из ответа сервера.
     * @param {HttpResponse} response Ответ сервера.
     */
    private extractData(response: HttpResponse): any {
        let body = JSON.parse(response.json()) as Response;
        return (body || {});
    }

}