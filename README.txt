��� prod ������� ����������:
1) �� ����� "\src\ImageEditor.Web.Client" ��������� � ������� ������� "npm install".
2) �� ����� "\src\ImageEditor.Web.Client" ��������� � ������� ������� "npm run build".
3) ��������� � Visual Studio ������ ��� ������ (Ctrl + F5).

��� dev ������� ����������:
1) �� ����� "\src\ImageEditor.Web.Client" ��������� � ������� ������� "npm install".
2) �� ����� "\src\ImageEditor.Web.Client" ��������� � ������� ������� "npm run start".
3) ��������� � Visual Studio ������ ��� ������� (F5).

--��� ��������, ������� prod-������� ��� ������ � �����������, ��� ������� ������� ���������� ��������� � Visual Studio 2017 ��� ������ (Ctrl + F5)--

--����������--

�.�. ������� ��������, ��������� ���� �� ���� ������� ��������, � ���������:
  1. ��������� ������� ���������� (����� ������ �������� �Ѩ). �.�. ���� �������� �������� ������� �� �����, � ����� ������� � �������� ��������,
     ����������� �� ������ �������. ��� ���������� ������� ���������� ����� ����:
     �) ���� ��� ��������� ������� ��������� ����������� � ����������� ����,
     �) ���� ������� ��� ������� ��������� ���� � �������������� (���, ��������, � ��������).
  2. �� ��������� ����� �� ���� �������� �������� ������������.
  3. ����������� ����-�����.
  4. �� ����� �������� ��������-��������� angular/material