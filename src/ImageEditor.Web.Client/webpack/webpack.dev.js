"use strict";

const webpackMerge = require('webpack-merge');
const webpackMergeDll = webpackMerge.strategy({ plugins: 'replace' });
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const DllBundlesPlugin = require('webpack-dll-bundles-plugin').DllBundlesPlugin;

const commonConfig = require('./webpack.common.js');
const helpers = require('./helpers');

const ENV = 'development';
const PORT = 8888;
const HOST = "localhost";

module.exports = webpackMerge(commonConfig({ env: ENV }), {
    devtool: 'cheap-module-eval-source-map',

    output: {
        path: helpers.root("Content/js/"),
        publicPath: '/Content/js/',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },

    plugins: [
        new ExtractTextPlugin('[name].css'),
    ],

    devServer: {
        contentBase: helpers.root(),
        host: HOST,
        port: PORT
    }
});
