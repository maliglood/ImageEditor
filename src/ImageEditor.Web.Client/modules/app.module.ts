﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from "../components/app.component";
import { ImageEditorComponent } from "../components/image-editor.component";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        NoopAnimationsModule
    ],
    declarations: [
        AppComponent,
        ImageEditorComponent
    ],
    bootstrap: [
        AppComponent
    ],
    entryComponents: [
    ]
})
export class AppModule { }
