﻿/**
 * Класс ответа с серверва.
 */
export default class Response {
    /**
     * Название файла.
     */
    public fileUrl: string;

    /**
     * Сообщение об ошибке.
     *
     */
    public error: string;
}