﻿namespace ImageEditor.Web.Controllers
{
    using System;
    using System.Web.Http;

    using Newtonsoft.Json;

    using ImageEditor.Web.Models;
    using ImageEditor.Web.Services;

    /// <summary>
    /// REST-сервис для изображений.
    /// </summary>
    [RoutePrefix("api/images")]
    public class ImagesController : ApiController
    {
        /// <summary>
        /// Сервис работы с изображениями.
        /// </summary>
        private ImagesService _imagesService = new ImagesService();

        /// <summary>
        /// Сохранение изображения.
        /// </summary>
        /// <param name="image"> Модель изображения. </param>
        /// <returns></returns>
        [HttpPut]
        [Route("")]
        public string Save(Image image)
        {
            var imageData = image.Data;
            var response = new Response();

            try
            {
                if (string.IsNullOrWhiteSpace(imageData))
                {
                    response.Error = "Файл не передан";
                }
                else
                {
                    response.FileUrl = _imagesService.Save(imageData);
                }
            }
            catch (Exception ex)
            {
                response.Error = ex.Message;
            }

            return JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
