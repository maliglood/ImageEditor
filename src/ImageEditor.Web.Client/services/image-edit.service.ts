﻿import { Injectable } from '@angular/core';

/**
 * Минимальное значение 8-ьитного канала.
 */
const CHANNEL_MIN_VALUE = 0;

/**
 * Максимальное значение 8-ьитного канала.
 */
const CHANNEL_MAX_VALUE = 255;

/**
 * Редактор изображений.
 */
export default class ImageEditService {
    /**
     * Фильтр "Сепия".
     * @param {HTMLCanvasElement} canvas Канва.
     * @param {CanvasRenderingContext2D} context Контекст 2D рендеринга для поверхности рисования канвы.
     */
    public sepia(canvas: HTMLCanvasElement, context: CanvasRenderingContext2D): ImageData {
        let imageData = context.getImageData(0, 0, canvas.width, canvas.height);
        let channels = imageData.data;

        for (var i = 0; i < channels.length; i += 4) {
            let r = channels[i];
            let g = channels[i + 1];
            let b = channels[i + 2];

            channels[i] = (r * 0.393) + (g * 0.769) + (b * 0.189); // red
            channels[i + 1] = (r * 0.349) + (g * 0.686) + (b * 0.168); // green
            channels[i + 2] = (r * 0.272) + (g * 0.534) + (b * 0.131); // blue
        }

        return imageData;
    }

    /**
     * Инвертирование цветов.
     * @param {HTMLCanvasElement} canvas Канва.
     * @param {CanvasRenderingContext2D} context Контекст 2D рендеринга для поверхности рисования канвы.
     */
    public invert(canvas: HTMLCanvasElement, context: CanvasRenderingContext2D): ImageData {
        let imageData = context.getImageData(0, 0, canvas.width, canvas.height);
        let channels = imageData.data;

        for (var i = 0; i < channels.length; i += 4) {
            channels[i] = 255 - channels[i];
            channels[i + 1] = 255 - channels[i + 1];
            channels[i + 2] = 255 - channels[i + 2];
        }

        return imageData;
    }

    /**
     * Изменение яркости.
     * @param {ImageData} imageData Пиксельные данные области канвы
     * @param {number} newBrightness Новое значение яркости
     */
    public changeBrightness(imageData: ImageData, newBrightness: number): ImageData {
        let newImageData = this.getOriginImageData(imageData);
        let channels = newImageData.data;

        for (var i = 0; i < channels.length; i += 4) {
            channels[i] = this.changeChannelValue(channels[i], newBrightness);
            channels[i + 1] = this.changeChannelValue(channels[i + 1], newBrightness);
            channels[i + 2] = this.changeChannelValue(channels[i + 2], newBrightness);
        }

        return newImageData;
    }

    /**
     * Изменение значение канала с учетом невыхода за границы допустимых значений.
     * @param {number} oldValue Старое значение канала.
     * @param {number} delta Велечина, на которую изменяется значение.
     */
    private changeChannelValue(oldValue: number, delta: number): number {
        let newValue = oldValue + delta;

        if (newValue > CHANNEL_MAX_VALUE) {
            newValue = CHANNEL_MAX_VALUE;
        } else if (newValue < CHANNEL_MIN_VALUE) {
            newValue = CHANNEL_MIN_VALUE;
        }

        return newValue;
    }

    /**
     * Получение яркости по формуле.
     * TODO: delete
     * @param rgb
     */
    private getLuminance(rgb: number[]) {
        var luminance = 0.2126 * rgb[0] + 0.7152 * rgb[1] + 0.0722 * rgb[2];
    }

    /**
     * Получение копии стартовых пиксельных данных области канвы.
     */
    private getOriginImageData(imageData: ImageData): ImageData {
        return new ImageData(imageData.data.slice(), imageData.width, imageData.height);
    }
}