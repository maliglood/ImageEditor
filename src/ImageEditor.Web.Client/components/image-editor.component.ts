﻿import { Component, ViewChild, OnInit } from "@angular/core";
import { DomSanitizer } from '@angular/platform-browser';

import ImageEditService from "../services/image-edit.service"
import ImagesService from "../services/images.service";

@Component({
    selector: 'image-editor',
    templateUrl: "./image-editor.component.html",
    styleUrls: ["./image-editor.component.scss"]
})

/**
 * Компонент редактирования изображения.
 */
export class ImageEditorComponent implements OnInit {
    /**
     * Ссылка на элемент канвы.
     */
    @ViewChild('canvas') canvasRef;

    /**
     * Ссылка на инпут загрузки файла.
     */
    @ViewChild('fileInput') fileInputRef;

    /**
     * Флаг сохранения файла на сервер.
     */
    public isSaving: boolean;

    /**
     * Значение яркости.
     */
    public brightness: number;

    /**
     * Предыдущее значение яркости.
     */
    public lastBrightness: number;

    /**
     * Ссылка на последний сохраненный файл.
     */
    public fileUrl: string;

    /**
     * Канва.
     */
    private _canvas: HTMLCanvasElement;

    /**
     * Инпут загрузки файла.
     */
    private _fileInput: HTMLInputElement;

    /**
     * Хелпер для загрузкци изображения.
     */
    private _fileReader: FileReader;

    /**
     * Контекст 2D рендеринга для поверхности рисования канвы.
     */
    private _context: CanvasRenderingContext2D;

    /**
     * Стартовые пиксельные данные области канвы.
     */
    private _originImageData: ImageData;

    /**
     * Конструктор.
     * @param {ImageEditService} _imageEditService Редактор изображений.
     * @param {ImagesService} _imagesService Сервис запросов для изображений.
     * @param {DomSanitizer} _domSanitizer 
     */
    constructor(private _imageEditService: ImageEditService, private _imagesService: ImagesService, private _domSanitizer: DomSanitizer) {
        this.reset();
        this._fileReader = new FileReader();
        this._fileReader.onloadend = this.fileReadEnd.bind(this);
    }

    /**
     * Инициализация компонента.
     */
    ngOnInit(): void {
        this._canvas = this.canvasRef.nativeElement;
        this._fileInput = this.fileInputRef.nativeElement;
        this._context = this._canvas.getContext('2d');
    }

    /**
     * Инициализация загрузки файла.
     */
    public upload(): void {
        this._fileInput.click();
    }

    /**
     * Загрузка файла на клиент.
     */
    public uploadEnd(): void {
        this.reset();
        let file = this._fileInput.files[0];

        if (file) {
            this._fileReader.readAsDataURL(file);
        };
    }

    /**
     * Сохранение файла на сервер.
     */
    public save(): void {
        this.isSaving = true;
        let imageDataString = this._canvas.toDataURL("image/png");

        this._imagesService
            .put(imageDataString)
            .subscribe(response => {
                if (response.error == null || response.error === "") {
                    alert('Изображение успешно сохранено');
                    this.fileUrl = window.location.host + "/" + response.fileUrl;
                } else {
                    alert(`Во время сохранения произошла ошибка: ${response.error}`);
                }

                this.isSaving = false;
            });
    }

    /**
     * Фильтр "Сепия".
     */
    public sepiaFilter(): void {
        let imageData = this._imageEditService.sepia(this._canvas, this._context);
        this._context.putImageData(imageData, 0, 0);
    }

    /**
     * Фильтр "Инвертирование".
     */
    public invertFilter(): void {
        let imageData = this._imageEditService.invert(this._canvas, this._context);
        this._context.putImageData(imageData, 0, 0);
    }

    /**
     * Изменение яркости изображения.
     */
    public brightnessFilter(): void {
        let imageData = this._imageEditService.changeBrightness(this.getImageData(), this.brightness - this.lastBrightness);
        this._context.putImageData(imageData, 0, 0);
        this.lastBrightness = this.brightness;
    }

    /**
     * Отмена изменений.
     */
    public cancel(): void {
        this.reset();
        this._context.putImageData(this._originImageData, 0, 0);
    }

    /**
     * Безопасное разрешение ссылки.
     * @param {string} url Ссылка.
     */
    public bypassSecurityTrustUrl(url: string): any {
        return this._domSanitizer.bypassSecurityTrustUrl(url);
    }


    /**
     * Событие окончания считывания изображения файлридером.
     */
    private fileReadEnd(): any {
        let image = new Image();
        image.src = this._fileReader.result;

        image.onload = () => { this.setImage(image); };

        image.remove();
    }

    /**
     * Получение пиксельных данных области канвы.
     */
    private getImageData(): ImageData {
        return this._context.getImageData(0, 0, this._canvas.width, this._canvas.height);
    }

    /**
     * Сброс всех параметров режактирования.
     */
    private reset(): void {
        this.isSaving = false;
        this.brightness = 0;
        this.lastBrightness = 0;
    }

    /**
     * Перенос изображения на канву.
     * @param {HTMLImageElement} image Загруженое изображение.
     */
    private setImage(image: HTMLImageElement) {
        let width: number;
        let height: number;

        let computedStyle = window.getComputedStyle(this._canvas.parentElement);
        let canvasWidth = parseInt(computedStyle.width, 10);
        let canvasHeight = parseInt(computedStyle.height, 10);

        let canvasRatio = canvasWidth / canvasHeight;
        let imgRatio = image.width / image.height;

        let isHorisontalOrientation = canvasRatio >= 1;

        if ((isHorisontalOrientation && (canvasRatio >= imgRatio)) ||
            (!isHorisontalOrientation && (canvasRatio <= imgRatio))) {
            height = canvasHeight;
            width = height * imgRatio;
        } else {
            width = canvasWidth;
            height = width / imgRatio;
        }

        this._canvas.width = width;
        this._canvas.height = height;

        this._canvas.style.width = width.toString() + "px";
        this._canvas.style.height = height.toString() + "px";

        this._context.drawImage(image, 0, 0, width, height);
        this._originImageData = this._context.getImageData(0, 0, this._canvas.width, this._canvas.height);
    }
}