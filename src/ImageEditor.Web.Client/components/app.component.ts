﻿import { Component } from "@angular/core";

import ImageEditService from "../services/image-edit.service";
import ImagesService from "../services/images.service";

@Component({
    selector: 'main',
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"],
    providers: [ImagesService, ImageEditService]
})

/**
 * Главный компонент (точка входа).
 */
export class AppComponent {
    /**
     * Конструктор.
     */
    constructor() { }
}