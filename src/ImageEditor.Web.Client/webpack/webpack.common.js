"use strict";

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin');

const helpers = require('./helpers');

module.exports = function (options) {
    return {
        entry: {
            'polyfills': './mains/polyfills.ts',
            'vendor': './mains/vendor.ts',
            'main': './mains/main.ts'
        },
        resolve: {
            extensions: ['.ts', '.js', '.json']
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    exclude: /node_modules/,
                    loaders: [
                        {
                            loader: 'awesome-typescript-loader',
                            options: { tsconfig: helpers.root('tsconfig.json') }
                        }, 'angular2-template-loader'
                    ]
                },
                { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader', query: { presets: ['es2015'] } },
                { test: /\.html/, loader: 'html-loader?minimize=false' },
                {
                    test: /\.scss$/,
                    exclude: /node_modules/,
                    use: [
                        { loader: 'to-string-loader' },
                        { loader: 'style-loader' },
                        { loader: 'css-loader' },
                        { loader: 'sass-loader' }
                    ]
                },
                {
                    test: /\.css/,
                    exclude: /node_modules/,
                    use: [
                        { loader: 'style-loader' },
                        { loader: 'css-loader' }
                    ]
                }
            ]
        },

        plugins: [
            new webpack.ContextReplacementPlugin(/\@angular(\\|\/)core(\\|\/)esm5/, helpers.root('./')),
            new CleanWebpackPlugin(),
            new webpack.DefinePlugin({
                ENV: JSON.stringify(options.env),
            }),
            new CommonsChunkPlugin({
                name: 'polyfills',
                chunks: ['polyfills']
            }),
            new CommonsChunkPlugin({
                name: 'vendor',
                chunks: ['main'],
                minChunks: module => /node_modules/.test(module.resource)
            }),
            new CommonsChunkPlugin({
                name: ['polyfills', 'vendor'].reverse()
            }),
        ]
    };
}