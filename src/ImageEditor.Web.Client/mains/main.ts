﻿import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from '../modules/app.module';

const platform = platformBrowserDynamic();
const bootApplication = () => { platform.bootstrapModule(AppModule); };

if (document.readyState === 'complete') {
    bootApplication();
} else {
    document.addEventListener('DOMContentLoaded', bootApplication);
}