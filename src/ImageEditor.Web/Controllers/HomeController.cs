﻿namespace ImageEditor.Web.Controllers
{
    using System.Web.Mvc;

    /// <summary>
    /// Главный MVC-контроллер.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Точка входа в приложение.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
    }
}
