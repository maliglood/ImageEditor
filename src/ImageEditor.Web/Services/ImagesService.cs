﻿namespace ImageEditor.Web.Services
{
    using System;
    using System.Web.Hosting;
    using System.Globalization;
    using System.IO;

    /// <summary>
    /// Сервис работы с изображениями.
    /// </summary>
    public class ImagesService
    {
        /// <summary>
        /// Название каталога с сохраненными изображениями.
        /// </summary>
        private const string FOLDER_NAME = "Images";

        /// <summary>
        /// Сохранение изображения на сервере.
        /// </summary>
        /// <param name="imageData"> Двоичные данные изображения. </param>
        /// <returns> Имя файла. </returns>
        public string Save(string imageData)
        {
            var name = DateTime.Now.ToString(CultureInfo.InvariantCulture).Replace("/", "-").Replace(" ", "- ").Replace(":", "") + ".png";
            var fullName = HostingEnvironment.MapPath($"~/{FOLDER_NAME}/") + name;

            using (FileStream fs = new FileStream(fullName, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    byte[] data = Convert.FromBase64String(imageData);
                    bw.Write(data);
                    bw.Close();
                }
            }

            return $"/{FOLDER_NAME}/{name}";
        }
    }
}