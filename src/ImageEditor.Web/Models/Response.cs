﻿namespace ImageEditor.Web.Models
{
    using Newtonsoft.Json;

    /// <summary>
    /// Ответ на сохранение изображения.
    /// </summary>
    public class Response
    {
        /// <summary>
        /// Название файла.
        /// </summary>
        [JsonProperty("fileUrl")]
        public string FileUrl { get; set; }

        /// <summary>
        /// Сообщение об ошибке.
        /// </summary>
        [JsonProperty("error")]
        public string Error { get; set; }
    }
}