﻿namespace ImageEditor.Web.Models
{
    using Newtonsoft.Json;

    /// <summary>
    /// Модель данных при запросе на сохранение изображения.
    /// </summary>
    public class Image
    {
        /// <summary>
        /// Двоичные данные изображения.
        /// </summary>
        [JsonProperty("imageData")]
        public string Data { get; set; }
    }
}